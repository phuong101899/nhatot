var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var multer  = require('multer');


var routes = require('./routes/index');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'uploads')));


var upload = multer({
    dest: './uploads/'
});


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


app.use('/', routes);

app.use('/:route',upload.any(), function (req, res, next) {
    if (req.params) {
        var route = req.params.route;
        try{
            if(req.xhr)
            {
                var controller = require('./routes/' + route);
                controller(req, res, next);
                
            }
            else
            {
                var controller = require('./routes/' + 'index');
                controller(req, res, next);
            }

        }
        catch (e)
        {
            console.log(e);
            var controller = require('./routes/' + 'index');
            controller(req, res, next);
        }

    }
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    res.render('index');
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.json('error', {
        message: err.message,
        error: {}
    });
});


var ipaddress = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
var port      = process.env.OPENSHIFT_NODEJS_PORT || 8080;

var server = app.listen(port,ipaddress, function () {
    var host = server.address().address;
    var port = server.address().port;
    //console.log(server);

    console.log('Example app listening at http://%s:%s', host, port);
});

//module.exports = app;
