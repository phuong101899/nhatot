/**
 * Created by phuongit on 05/03/16.
 */
var connection = require('../models/db');
var imagesModel = require('../models/images');
var Q = require('q');
var async = require("async");




var article = {};



article.get = function (params,total) {
    var category = params.category;
    
    var page = params.page || 1;
    var limit = params.limit || 10;
    var offset = (page - 1)*limit;

    var deferer = Q.defer();

    var selector = total ? 'count(*) as total' : 'article.*,categories.name as category_name';

    var query = ['SELECT',selector,'FROM article left join categories on categories.id = article.category_id'];

    if(category && category !== 'search')
    {

        query.push('where categories.alias = "'+category+'"');
    }

    if(category && category == 'search')
    {
        var where = false;
        if(params.location)
        {
            query.push('left join location on location.id = article.location_id');

            var location = params.location.split(',');

            var location = "'"+location.join("','")+"'";
            query.push('where location.`key` in ('+location+')');
            where = true;
        }
        if(params.kind)
        {
            var kind = params.kind.split(',');

            var parse_kind = "'"+kind.join("','")+"'";

            var prefix = !where ? 'where' : 'and';
            query.push(prefix + ' '+ 'article.kind in ('+parse_kind+')');
            where = true;
        }
        if(params.keyword)
        {
            var prefix = !where ? 'where' : 'and';
            query.push(prefix + ' '+ 'article.title like "%'+params.keyword+'%"');

        }
    }
    
    if(!total)
    {
        query.push('limit '+offset+','+limit);
    }

    query = query.join(' ');

    connection.query(query, function (err, rows, fields) {
        if (err)
        {
            deferer.resolve(false);
        }
        else
        {
            if(total)
            {
                var _total = rows[0];
                deferer.resolve(_total['total']);
            }
            else
            {
                var result = [];
                async.forEach(rows,function(item,callback){
                    imagesModel.get('slide_article',item.id).then(function(images){
                        item.images = images;
                        result.push(item);
                        callback();
                    });

                },function(err){
                    if(err) console.log(err);
                    deferer.resolve(result);
                });
            }



        }
    });
    return deferer.promise;
};


article.getByUserId = function(userId)
{
    if(userId)
    {
        var deferer = Q.defer();
        var query = ['SELECT * from article'];

        query.push('where user_id='+userId);
        query = query.join(' ');

        connection.query(query, function (err, rows, fields) {
            if (err)
            {
                deferer.resolve(false);
            }
            else
            {
                deferer.resolve(rows);
            }
        });
        return deferer.promise;
    }
    return false;
};


article.getDetail = function(id)
{
    var deferer = Q.defer();

    var query = ['SELECT * from article where id='+id];
    query = query.join(' ');

    connection.query(query, function (err, rows, fields) {
        if (err)
        {
            deferer.resolve(false);
        }
        else
        {
            var item = rows[0];

            Q.all([imagesModel.get('slide_article',id), imagesModel.get('drawing_article',id)])
            .spread(function (images, drawing) {
                item.images = images;
                item.drawing = drawing;

                deferer.resolve(item);
            }).done();

        }
    });
    return deferer.promise;
};



article.add = function(data)
{
    var deferer = Q.defer();

    connection.query('INSERT INTO article SET ?',data, function (err, result) {
        if (err)
        {
            console.log(err);
            deferer.resolve(false);
        }
        else
        {
            deferer.resolve(result.insertId);
        }
    });

    return deferer.promise;
};

article.update = function (data,where)
{
    var deferer = Q.defer();
    connection.query('UPDATE article SET ? WHERE user_id=? and id=?',[data,where.user_id,where.id],function(err,result){
        if (err)
        {
            console.log(err);
            deferer.resolve(false);
        }
        else
        {
            deferer.resolve(true);
        }
    });
    return deferer.promise;
};

article.delete = function(id)
{
    var deferer = Q.defer();
    if(id)
    {
        connection.query('DELETE FROM article WHERE id = ?',[id], function (err, result) {
            if (err)
            {
                console.log(err);
                deferer.resolve(false);
            }
            else
            {
                deferer.resolve(result.affectedRows);
            }
        });
    }
    else
    {
        deferer.resolve(false);
    }

    return deferer.promise;
};

module.exports = article;