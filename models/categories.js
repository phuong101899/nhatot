/**
 * Created by phuongit on 09/03/2016.
 */
var connection = require('../models/db');
var Q = require('q');
var async = require("async");


var categories = {};

categories.get = function()
{
    var deferer = Q.defer();

    var query = ['SELECT * from categories order by `order`'];
    query = query.join(' ');

    connection.query(query, function (err, rows, fields) {
        if (err)
        {
            deferer.resolve(false);
        }
        else
        {
            deferer.resolve(rows);
        }
    });
    return deferer.promise;
}

module.exports = categories;