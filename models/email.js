var connection = require('../models/db');
var Q = require('q');
var async = require("async");


var models = {};

models.add = function(data)
{
    var deferer = Q.defer();

    connection.query('INSERT INTO email SET ?',[data], function (err, result) {
        if (err)
        {
            //console.log(err);
            deferer.resolve(false);
        }
        else
        {
            deferer.resolve(result.insertId);
        }
    });

    return deferer.promise;
};


module.exports = models;