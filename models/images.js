/**
 * Created by phuongit on 23/05/2016.
 */
var connection = require('../models/db');
var Q = require('q');
var async = require("async");


var $this = {};

$this.get = function(table,id_article)
{
    var deferer = Q.defer();

    var query = ['SELECT * from ' + table];
    query.push('WHERE id_article = "'+id_article+'"');

    query = query.join(' ');

    connection.query(query, function (err, rows, fields) {
        if (err)
        {
            deferer.reject(false);
        }
        else
        {
            deferer.resolve(rows);

        }
    });
    return deferer.promise;
}

$this.add = function(table,data)
{
    var deferer = Q.defer();

    connection.query('INSERT INTO '+table+' SET ?',[data], function (err, result) {
        if (err)
        {
            console.log(err);
            deferer.resolve(false);
        }
        else
        {
            deferer.resolve(result.insertId);
        }
    });

    return deferer.promise;
};

$this.delete = function(table,id_image)
{
    var deferer = Q.defer();

    if(typeof  id_image == 'string') id_image = [id_image];

    if(id_image && id_image.length > 0)
    {
        var id = id_image.join(',');

        var sql = 'DELETE FROM '+table+' WHERE id IN ('+id+')';

        connection.query(sql, function (err, result) {
            if (err)
            {
                console.log(err);
                deferer.resolve(false);
            }
            else
            {
                deferer.resolve(result.affectedRows);
            }
        });
    }
    else
    {
        deferer.resolve(false);
    }
    return deferer.promise;
};

module.exports = $this;