var connection = require('../models/db');
var Q = require('q');


var location = {};



location.get = function () {
    var deferer = Q.defer();

	connection.query('SELECT * from location', function (err, rows, fields) {
		if (err) 
        {
			deferer.resolve(false);
		}
        else
        {
            deferer.resolve(rows);
        }
	});

    return deferer.promise;
}


module.exports = location;