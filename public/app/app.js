(function(window, angular, $, undefined){
    'use strict';
    var home = 'home';

    var app = angular.module('home', ['ui.router', 'ngCookies','wu.masonry']);

    app.config(function ($stateProvider, $urlRouterProvider,$httpProvider,$locationProvider) {
        //
        // For any unmatched url, redirect to /state1
        $urlRouterProvider.otherwise("/index.html");
        // remove # at url
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
        // Now set up the states
        $stateProvider
            .state(home, {
                url: "",
                templateUrl: "/app/templates/home.html",
                controller : "baseCtrl",
                abstract : true,
                resolve : {
                    location : ['API',function(API){
                        return API.getLocation().success(function(res){
                            return res.rows;
                        });
                    }]
                }
            })
            .state([home,'detail'].join('.'), {
                url: "/xem-:alias-:id.html",
                templateUrl: "/app/templates/detail.html",
                controller : 'detailController'
            })
            .state([home,'write'].join('.'), {
                url: "/write.html",
                templateUrl: "/app/templates/write.html",
                controller : 'writeController'
            })
            .state([home,'login'].join('.'), {
                url: "/login.html",
                template : '',
                controller : 'loginController'
            })
            .state([home,'update'].join('.'), {
                url: "/update-:id.html",
                templateUrl: "/app/templates/write.html",
                controller : 'writeController'
            })
            .state([home,'user'].join('.'), {
                url: "/user-page.html",
                templateUrl: "/app/templates/user-page.html",
                controller : 'userController'
            })
            .state([home,'index'].join('.'), {
                url: "/:category.html?location&keyword&kind&page",
                templateUrl: "/app/templates/index.html",
                controller : 'indexController'
            });




        $httpProvider.interceptors.push(['$rootScope',
            function ($rootScope) {
                return {
                    'request': function (config) {
                        $rootScope.isBlocked = true;
                        if (config.hasOwnProperty('block') && config.block!=false) {
                            $rootScope.isBlocked = true;
                        }

                        return config;
                    },
                    'response': function (response) {
                        $rootScope.isBlocked = false;
                        return response;
                    }
                }
            }]);

        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    });


    

    app.service('API', ['$http','$q','$rootScope', function ($http,$q,$rootScope) {
        this.signUp = function (data) {
            return $http.post('/users', data);
        };
        this.login = function (data) {
            return $http.put('/users', data);
        };
        this.getLocation = function()
        {
            return $http.get('/location',{cache : true});
        };
        this.addArticle = function(data)
        {
            $rootScope.isBlocked = true;
            var deferer = $q.defer();
            $.ajax({
                url: '/article',
                type: 'POST',
                data: data,
                cache: false,
                contentType: false,
                processData: false
            }).done(function(res){
                deferer.resolve(res);
                $rootScope.isBlocked = false;
            });

            return deferer.promise;
        };

        this.updateArticle = function (data)
        {
            $rootScope.isBlocked = true;
            var deferer = $q.defer();
            $.ajax({
                url: '/article',
                type: 'PUT',
                data: data,
                cache: false,
                contentType: false,
                processData: false
            }).done(function(res){
                deferer.resolve(res);
                $rootScope.isBlocked = false;
            });

            return deferer.promise;
        };


        this.getArticle = function(params)
        {
            return $http.get('/article',{params:  params});
        };

        this.deleteArticle = function(params)
        {
            return $http.delete('/article',{data : params});
        };

        this.getCategories = function()
        {
            return $http.get('/categories',{cache : true});
        };

        this.getDetailArticle = function(params)
        {
            return $http.get('/article',{params : params});
        };

        this.getUserArticle = function(params)
        {
            return $http.get('/users',{params : params});
        };

        this.addEmail = function(params)
        {
            return $http.post('/email',params);
        };

        return this;
    }]);


    app.directive('preloader', [function () {
        return {
            templateUrl: '/app/templates/preloader.html'
        }
    }]);


    app.directive('ckEditor', [function () {
        return {
            require: '?ngModel',
            link: function ($scope, elm, attr, ngModel) {

                // CKEDITOR.plugins.addExternal('ngen_filebrowse', '/js/','filebrowser.js');

                var ck = CKEDITOR.replace(elm[0],{
                    uiColor : '#33B5E5',
                    toolbar:'Basic',
                    language: 'vi',
                });

                if (!ngModel) return;

                ck.on('instanceReady', function() {
                    ck.setData(ngModel.$viewValue);
                });


                ck.on('pasteState', function () {
                    $scope.$apply(function () {
                        ngModel.$setViewValue(ck.getData());
                    });
                });


                

                ngModel.$render = function (value) {
                    ck.setData(ngModel.$modelValue);
                };
            }
        };
    }]);
    
    app.directive('sideNav',[function(){
        return {
            link : function($scope, elm, attr, ngModel)
            {
                elm.sideNav({
                    menuWidth: 300, // Default is 240
                    edge: 'right', // Choose the horizontal origin
                    closeOnClick: true
                });
            }
        }
    }]);
    
    app.directive('searchNav',['API','$timeout',function(API,$timeout){
        return {
            scope : {},
            templateUrl: '/app/templates/search-nav.html',
            link : function($scope, elm)
            {

                $scope.search = {
                    location : ['quan1'],
                    kind : ['buy','sell']
                };

                $scope.searchAction = function (data) {
                    $scope.$emit('searchAction',data);
                };

                API.getLocation().success(function(res){
                    if(res.success)
                    {
                        $scope.locationList = res.rows;

                        $timeout(function() {
                            elm.find('select').material_select();
                        },0);
                    }

                });
                
                
            }
        }
    }]);

    app.directive('materialSelect',['$timeout',function($timeout) {
        return {
            link : function  ($scope, elm, attr, ngModel) {

                function build()
                {
                    $timeout(function() {
                        elm.material_select();
                    },0);
                }

                build();

                $scope.$on('reloadSelect',function(event,data){
                    build();
                });
                
            }
        }
    }]);



    app.directive('address',[function(){
        return {
            require : '?ngModel',
            link : function  ($scope, elm, attr, ngModel) {
                if(typeof google === "undefined")
                {
                    var url = "https://maps.googleapis.com/maps/api/js?key=AIzaSyA3hpH9hgP4uwZL-9DzyLiXevg4WMwyT_Y&signed_in=true&libraries=places";
                    $.getScript(url).done(function(res){
                        action();
                    });
                }
                else
                {
                    action();
                }

                function action()
                {
                    var input = angular.element(elm).get(0);
                    var autocomplete = new google.maps.places.Autocomplete(input, {types: ['geocode']});
                    autocomplete.addListener('place_changed',function(){
                        var place = elm.val();
                        $scope.$apply(function () {
                            ngModel.$setViewValue(place);
                        });
                    });
                }

                
            }
        }
    }]);

    app.directive('googleMap',['$http',function($http){
        return {
            require : '?ngModel',
            link : function  ($scope, elm, attr, ngModel) {
                var address = encodeURI(attr.googleAddress);

                var url = "http://maps.googleapis.com/maps/api/geocode/json?address="+address+"&sensor=true";

                $.get(url).then(function(res){
                    try{
                        var location = res.results[0].geometry.location;
                    }catch(e){
                        var location = {lat:10.0451618, lng: 105.7468535};
                    }

                    if(typeof google === "undefined")
                    {
                        var url = "https://maps.googleapis.com/maps/api/js?key=AIzaSyA3hpH9hgP4uwZL-9DzyLiXevg4WMwyT_Y&signed_in=true";
                        $.getScript(url).done(function(res){
                            action(location);
                        });
                    }
                    else
                    {
                        action(location);
                    }
                });


                
                function action(location)
                {
                    var input = angular.element(elm).get(0);
                    var option = {
                        center: location,
                        zoom: 12
                    };
                    var map = new google.maps.Map(input,option);

                    new google.maps.Marker({
                        position : location,
                        map : map
                    });
                }

            }
        }
    }]);

    app.directive('ngSlider',['$timeout',function($timeout){
        return {
            link : function($scope, elm, attr, ngModel)
            {
                $timeout(function(){
                    elm.slider({
                        indicators : false,
                        height : 300
                    });
                },0);
            }
        }
    }]);
    app.directive('checklistModel', ['$parse', '$compile', function ($parse, $compile) {
        // http://vitalets.github.io/checklist-model/
        // contains
        function contains(arr, item) {
            if (angular.isArray(arr)) {
                for (var i = 0; i < arr.length; i++) {
                    if (angular.equals(arr[i], item)) {
                        return true;
                    }
                }
            }
            return false;
        }

        // add
        function add(arr, item) {
            arr = angular.isArray(arr) ? arr : [];
            for (var i = 0; i < arr.length; i++) {
                if (angular.equals(arr[i], item)) {
                    return arr;
                }
            }
            arr.push(item);
            return arr;
        }

        // remove
        function remove(arr, item) {
            if (angular.isArray(arr)) {
                for (var i = 0; i < arr.length; i++) {
                    if (angular.equals(arr[i], item)) {
                        arr.splice(i, 1);
                        break;
                    }
                }
            }
            return arr;
        }

        // http://stackoverflow.com/a/19228302/1458162
        function postLinkFn(scope, elem, attrs) {
            // compile with `ng-model` pointing to `checked`
            $compile(elem)(scope);

            // getter / setter for original model
            var getter = $parse(attrs.checklistModel);
            var setter = getter.assign;

            // value added to list
            var value = $parse(attrs.checklistValue)(scope.$parent);

            // watch UI checked change
            scope.$watch('checked', function (newValue, oldValue) {
                if (newValue === oldValue) {
                    return;
                }
                var current = getter(scope.$parent);
                if (newValue === true) {
                    setter(scope.$parent, add(current, value));
                } else {
                    setter(scope.$parent, remove(current, value));
                }
            });

            // watch original model change
            scope.$parent.$watch(attrs.checklistModel, function (newArr, oldArr) {
                scope.checked = contains(newArr, value);

                // Dinh Tran added 2014-07-02
                // Add and remove class selected
                if (scope.checked) {
                    $(elem).closest('.message-item').addClass('selected');
                    $(elem).closest('.message-item-photo').addClass('selected');
                    $(elem).closest('li').addClass('checked'); // for Note item (Hung Le 2014/07/29)
                } else {
                    $(elem).closest('.message-item').removeClass('selected');
                    $(elem).closest('.message-item-photo').removeClass('selected');
                    $(elem).closest('li').removeClass('checked'); // for Note item (Hung Le 2014/07/29)
                }
            }, true);
        }

        return {
            restrict: 'A',
            priority: 1000,
            terminal: true,
            scope: true,
            compile: function (tElement, tAttrs) {
                if (tElement[0].tagName !== 'INPUT' || !tElement.attr('type', 'checkbox')) {
                    throw 'checklist-model should be applied to `input[type="checkbox"]`.';
                }

                if (!tAttrs.checklistValue) {
                    throw 'You should provide `checklist-value`.';
                }

                // exclude recursion
                tElement.removeAttr('checklist-model');

                // local scope var storing individual checkbox model
                tElement.attr('ng-model', 'checked');

                return postLinkFn;
            }
        };
    }]);
    
    app.directive('buildMenus',['API','$rootScope',function(API,$rootScope) {
        return {
            restrict : 'AE',
            templateUrl : ['app','templates','menu.html'].join('/'),
            link : function ($scope, elm, attr, ngModel) {
                //get data menu
                if(!angular.isArray($rootScope.menuList) || $rootScope.menuList.length < 1)
                {
                    API.getCategories().success(function(res){
                        $rootScope.menuList = res.rows;
                    });
                }
                                
            }
        }
    }]);
    
    app.directive('childrenMenu',['$timeout','$location','$state',function($timeout,$location,$state) {
        return {
            restrict : 'AE',
            scope : {
                menu : '=',
                menuId : '='
            },
            templateUrl : ['app','templates','menu-dropdown.html'].join('/'),
            link : function (scope, elm, attr, ngModel) {
                 $timeout(function () {
                      $('.dropdown-button').dropdown({
                          constrain_width: false
                      });
                     
                     $('.collapsible').collapsible({
                        accordion : true
                     });
                 },0);
                 
                 
                 scope.preventDefault = function(e) 
                 {
                     e.preventDefault();
                     e.stopPropagation();
                 };


                scope.go = function(state,params)
                {
                    $location.$$search = {};
                    $state.go(state,params,{location : 'replace',inherit: false,reload : true});

                };
                 
            }
        }
    }]);
    app.directive('tooltip',['$timeout',function($timeout){
        return {
            link : function  ($scope, elm) {
                elm.tooltip();
            }
        }
    }]);


    /* Controller section */
    app.controller('baseCtrl', ['$scope', 'API', '$cookies', '$cookieStore', '$sce','$state','$location','$rootScope','$window',
        function ($scope, API, $cookies, $cookieStore, $sce, $state, $location,$rootScope,$window) {

        $rootScope.document = document;

        $scope.isLoginForm = true;
        $scope.getSignupForm = function () {
            $scope.isLoginForm = false;
        };
        $scope.getLoginForm = function () {
            $scope.isLoginForm = true;
        };

        $scope.form = {};
        $scope.signup = function (data) {
            API.signUp(data).success(function (res) {
                console.log(res);
                Materialize.toast(res.msg, 4000, 'center-align');
                if (res.success) {
                    $scope.form = {};
                    $scope.getLoginForm();
                }
            });
        };

        $scope.login = function (data) {
            API.login(data).success(function (res) {
                Materialize.toast(res.msg, 4000, 'center-align');
                if (res.success) {
                    $scope.form = {};
                    $('#user-panel').closeModal();
                }
            });
        };

        $scope.logout = function()
        {
            $cookies.remove('USER_LOGIN');
        };


        function isLogin() {
            var USER_LOGIN = $cookies.get('USER_LOGIN');
            if (USER_LOGIN) USER_LOGIN = JSON.parse(USER_LOGIN);
            return USER_LOGIN;

        }

        $scope.isLoggedin = function () {
            if(angular.isObject(isLogin()))
            {
                return true;
            }
            return false;
        };
        
        $scope.openModal = function(id)
        {
            $(id).openModal();
        };

         $scope.formatHTML = function(string)
         {
             if(!string) return '';
             return $sce.trustAsHtml(string);
         };


          $scope.searchTagKind = function(kind)
          {
              var params = {
                  kind : [kind]
              };

              $scope.searchAction(params);
          };


         $scope.searchAction = function(data)
         {
             $('#sidenav-overlay').trigger('click'); // hide overlay search

             var params = {};
             angular.forEach(data,function(item,index){
                 if(angular.isArray(item))
                 {
                     var temp = angular.copy(data[index].join(','));
                     params[index] = temp;
                 }
                 else
                 {
                     params[index] = item;
                 }

             });

             params.category = 'search';

             $state.go('home.index',params,{location : 'replace',inherit: false,reload : true});
         };

         $scope.$on('searchAction',function(event,data){
             $scope.searchAction(data);
         });


        $scope.go = function(state,params)
        {
            $location.$$search = {};
            $state.go(state,params,{location : 'replace',inherit: false,reload : true});

        };


        $scope.emailForm = {};

        $scope.addEmail = function(email)
        {
            var data = { email : email };

            API.addEmail(data).success(function(res){
                if(res.success)
                {
                    $scope.emailForm.email = null;
                }
                Materialize.toast(res.msg, 4000, 'center-align');
            });
        };

        $scope.$on('$viewContentLoaded', function(event) {
            $window.ga('send', 'pageview', { page: $location.url() });
        });
    }]);
    app.controller('writeController',['$scope','$timeout','API','location','$state','$stateParams','$rootScope',
    function($scope,$timeout,API,location,$state,$stateParams,$rootScope){

        var id = $stateParams.id;
        $scope.form = {};


        $scope.imageList = [];
        $scope.imageSlideList = [];

        function getDataUpdate(id)
        {
            if(id)
            {
                return API.getArticle({id:id}).success(function(res){
                    $scope.form = res.rows;
                    $scope.form.category_id = res.rows.category_id.toString();
                    $scope.$broadcast('reloadSelect'); // rebuild materialSelect

                    if($scope.form.images.length > 0)
                    {
                        for(var i in $scope.form.images)
                        {
                            var img = $scope.form.images[i];
                            $scope.imageSlideList.push({src : img.name,id : img.id});
                        }

                        $timeout(function() {
                            $(".slider").slider();
                        },0);
                    }

                    if($scope.form.drawing.length > 0)
                    {
                        for(var i in $scope.form.drawing)
                        {
                            var img = $scope.form.drawing[i];
                            $scope.imageList.push({src : img.name, id : img.id});
                        }
                    }


                    $timeout(function(){
                        Materialize.updateTextFields();
                    },0);
                });
            }
        }

        getDataUpdate(id);



        $scope.location = location.data.rows;

        function uploadImagetoCloud(image)
        {
            var auth = 'Client-ID f2f616efac9cca1';

            var formData = new FormData();
            formData.append('image',image);


            return $.ajax({
                url: 'https://api.imgur.com/3/image',
                type: 'POST',
                headers: {
                    Authorization: auth,
                    Accept: 'application/json'
                },
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend : function () {
                    $timeout(function () {
                        $rootScope.isBlocked = true;
                    },0);
                },
                success: function(result) {
                    $timeout(function () {
                        $rootScope.isBlocked = false;
                    },0);
                }
            });
        }

        $(document).on('change','#image-file',function(event){

            var $this = $(this);
            var file = event.target.files[0];

            uploadImagetoCloud(file).done(function (res) {
                var link  = res.data.link;

                $scope.imageList.push({src:link});
                $this.val('');
            });

        });

        $(document).on('change','#image-slide',function(event){

            var $this = $(this);
            var file = event.target.files[0];

            uploadImagetoCloud(file).done(function (res) {
                var link  = res.data.link;

                $scope.imageSlideList.push({src:link});
                $this.val('');
            });
        });

        $scope.removeSlideImage = function($index)
        {
            $scope.imageSlideList.splice($index,1);
        };

        $scope.removeImage = function($index)
        {
            $scope.imageList.splice($index,1);
        };

        API.getCategories().success(function(res){
            $scope.categories = res.rows;
            if(!id) $scope.form.category_id = defaultCategory(res.rows);
        });

        function defaultCategory(categories)
        {
            if(angular.isArray(categories) && categories.length > 0)
            {
                var first_item = categories[0];
                if(!first_item.children)
                {
                    return first_item.id.toString();
                }
                else
                {
                    return defaultCategory(first_item.children);
                }
            }

        }


        $scope.saveArticle = function(data)
        {
            if(data.id) return $scope.updateArticle(data);

            var formData = new FormData();
            angular.forEach($scope.imageList,function(item,i){
                formData.append('images',item.src);
            });
            angular.forEach($scope.imageSlideList,function(item,i){
                formData.append('slide',item.src);
            });

            for(var key in data)
            {
                formData.append(key,data[key]);
            }

            API.addArticle(formData).then(function(res){
                if(!res.success)
                {
                    return sweetAlert("Oops...", "Lưu bài viết thất bại!", "error");
                }
                $state.go([home,'index'].join('.'),{category : 'index'});
            });
        };

        $scope.updateArticle = function(data)
        {
            var formData = new FormData();
            for(var key in data)
            {
                formData.append(key,data[key]);
            }

            var drawing = [],slide = [];

            angular.forEach($scope.imageList,function(item,i){
                if(item.file)
                {
                    formData.append('image',item.file);
                }
                else
                {
                    drawing.push(item.id);
                }
            });
            angular.forEach($scope.imageSlideList,function(item,i){
                if(item.file)
                {
                    formData.append('slide',item.file);
                }
                else
                {
                    slide.push(item.id);
                }
            });


            for (var i in data.drawing)
            {
                var item = data.drawing[i];
                if(drawing.indexOf(item.id) == -1)
                {
                    formData.append('drawing_delete',item.id);
                }
            }

            for (var i in data.images)
            {
                var item = data.images[i];
                if(slide.indexOf(item.id) == -1)
                {
                    formData.append('slide_delete',item.id);
                }
            }

            API.updateArticle(formData).then(function(res){
                if(res.success)
                {
                    $state.go([home,'user'].join('.'));
                }
                else
                {
                    sweetAlert(res.msg);
                }
            });
        };




    }]);

    app.controller('indexController',['API','$scope', '$timeout','$stateParams','$location','$state',
    function(API,$scope,$timeout,$stateParams,$location,$state){
        $scope.currentPage = parseInt($location.$$search.page) || 1;

        function getList()
        {
            var params = {};
            var category = $stateParams.category;
            if(category && category !='index')
            {
                params = {category : category};
            }

            if(category == 'search')
            {
                params = $.extend(params,$location.$$search);
            }

            params.page = $scope.currentPage;

            API.getArticle(params).success(function(res){
                $scope.listArticle = res.rows;
                $scope.totalPage = res.totalPage;
                $scope.paseList = parsePageList($scope.totalPage);
            });
        }

        getList();

        function parsePageList(totalPage)
        {
            var list = [];

            if($scope.currentPage == 1)
            {
                list.push($scope.currentPage,$scope.currentPage + 1,$scope.currentPage + 2);
            }
            else if($scope.currentPage == totalPage && totalPage >= 3)
            {
                list.push($scope.currentPage - 2,$scope.currentPage - 1,$scope.currentPage);
            }
            else
            {
                list.push($scope.currentPage - 1,$scope.currentPage,$scope.currentPage + 1);
            }

            if(totalPage < 3)
            {
                list.splice(totalPage,3-totalPage);
            }

            return list;
        }


        $scope.changePage = function(page)
        {
            var params = $stateParams;

            params.page = page;
            $state.go('home.index',params,{location : 'replace',inherit: false,reload : true});
        }
    }]);


    app.controller('detailController',['$scope','$stateParams','API',function($scope,$stateParams,API){
        var id = $stateParams.id;
        if(id && parseInt(id) > 0)
        {
            API.getDetailArticle({id:id}).success(function(res){
                console.log(res);
                $scope.details = res.rows;
            });
        }
    }]);
    app.controller('userController',['$scope','API',function($scope,API){
        getArticleList();

        function getArticleList()
        {
            var params = {
                type : 'article_list'
            };
            $scope.listItems = [];
            API.getUserArticle(params).success(function(res){
                try{
                    if(res.success)
                    {
                        $scope.listItems = res.rows;
                        console.log($scope.listItems);
                    }
                    else
                    {
                        sweetAlert(res.msg);
                    }

                }catch (e){
                    console.log(e);
                }
            });
        }


        $scope.deleteArticle = function(id,key)
        {
            sweetAlert({
                title: "Bạn muốn xóa bài viết này?",
                //text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Xác nhận!",
                cancelButtonText: "Hủy",
                closeOnConfirm: true },
                function(){
                    deleteAPI(id);
                });

            function deleteAPI(id)
            {
                var formData = new FormData();
                formData.append('id',id);
                API.deleteArticle(formData).success(function(res){
                    if(res.success)
                    {
                        $scope.listItems.splice(key,1);
                    }
                    else
                    {
                        sweetAlert(res.msg);
                    }

                });
            }
        };
    }]);

    app.controller('loginController',['$scope',function($scope) {
        $('#user-panel').openModal();
    }]);
})(window, window.angular, window.jQuery);












