$(document).ready(function() {
    $('select').material_select();
    $(".button-collapse").sideNav({
    	menuWidth: 300, // Default is 240
      	edge: 'right', // Choose the horizontal origin
      	closeOnClick: true
    });

    $('.modal-trigger').leanModal();
    $('ul.tabs').tabs();
});