var fs = require('fs');
var articleModel = require('../models/article');
var imagesModel = require('../models/images');
var StringJS = require('string');
var Q = require('q');
var util = require('util');


function addImage(data,id_article,table)
{

    if(!util.isArray(data))
    {
        data = [data];
    }

    for(var i in data)
    {        
        var insert_data = {
            name : data[i],
            origin_name : 'originalname',
            id_article : id_article,
        };

        addImageToDb(table,insert_data);
    }
}


function addImageToDb(table,data)
{
    imagesModel.add(table,data);
}

var article = function(req, res, next)
{
    switch (req.method) {
        case 'GET': {
            var id = req.query.id;
            if(id)
            {
                return getDetail(id,res);
            }

            //console.log(req.cookies);
            // getList

            req.query.page = req.query.page || 1;
            req.query.limit = req.query.limit || 10;


            Q.all([articleModel.get(req.query,true), articleModel.get(req.query)])
            .spread(function (total, rows) {
                if(rows)
                {

                    var totalPage = Math.ceil(total/req.query.limit);

                    res.json({success : true, rows : rows, total : total, totalPage : totalPage});
                }
                else
                {
                    res.json({success : false, msg : 'Error!'});
                }
            }).done();



            /*articleModel.get(req.query).then(function(rows){
                if(rows)
                {

                    res.json({success : true, rows : rows});
                }
                else
                {
                    res.json({success : false, msg : 'Error!'});
                }
            });*/
            break;
        }
        case 'POST': {

            try{
                var user_data = JSON.parse(req.cookies['USER_LOGIN']);
            }catch (e){
                return res.json({success : false, msg : "Can't not permission to access!"});
            }

            var data = req.body;

            data.alias = StringJS(data.title).latinise().trim().replaceAll(" ", "-").replaceAll(",", "").toLowerCase();

            data.user_id = user_data.id;

            var drawing_article = data.images; delete data.images;
            var slide_article = data.slide; delete data.slide;

            articleModel.add(data).then(function(result){
                if(result)
                {
                    var id_article = result;
                    addImage(drawing_article,id_article,'drawing_article');
                    addImage(slide_article,id_article,'slide_article');
                }

                res.json({success : true, id : id_article, data : data});
            },function(err)
                {
                    res.json({success : false, msg : 'Error!'});
                }
            );





            break;
        }

        case 'PUT':{
            try{
                var user_data = JSON.parse(req.cookies['USER_LOGIN']);
            }catch (e){
                return res.json({success : false, msg : "Can't not permission to access!"});

            }

            var data = req.body;

            var id = data.id;
            if(!id)
            {
                return res.json({success : false, msg : 'Missing id to update!'});
            }



            var update_data = {
                title : data.title,
                location_id : data.location_id,
                category_id : data.category_id,
                kind : data.kind,
                price : data.price,
                contents : data.contents,
                address : data.address
            };

            var files = req.files;



            var images = [];
            var slides = [];

            try
            {
                for (var i in files)
                {
                    var item = files[i];
                    if(item.fieldname == 'slide')
                    {
                        slides.push(item);
                    }
                    else if(item.fieldname == 'image')
                    {
                        images.push(item);
                    }
                }

            }catch (e)
            {
                console.log(e);
            }





            update_data.alias = StringJS(update_data.title).latinise().trim().replaceAll(" ", "-").toLowerCase();

            var where = {user_id : user_data.id,id : id};
            articleModel.update(update_data,where).then(function(result){
                if(result)
                {
                    addImage(images,id,'drawing_article');
                    addImage(slides,id,'slide_article');

                    try{
                        if(typeof data.slide_delete !=='undefined')
                        {
                            imagesModel.delete('slide_article',data.slide_delete);
                        }

                        if(typeof data.drawing_delete !=='undefined')
                        {
                            imagesModel.delete('drawing_article',data.drawing_delete);
                        }
                    }catch (e){
                        console.log(e);
                    }


                }

                res.json({success : true, msg : 'Update successfully!'});
            },function(err){
                res.json({success : false, msg : 'Error!'});
            });

            break;
        }
        case 'DELETE': {
            var data = req.body;

            articleModel.delete(data.id).then(function(result){
                if(result) res.json({success : true, data : data,status : result});
                else res.json({success : false, data : data,msg : 'Deleted failed!'});
            });

            break;
        }
    }

    function getDetail(id,res)
    {

        articleModel.getDetail(id).then(function(result){
            //console.log(result);

            if(result)
            {
                res.json({success : true, rows : result});
            }
            else
            {
                res.json({success : false, mgs : 'Error!'});
            }

        });
    }
}

module.exports = article;