/**
 * Created by phuongit on 09/03/2016.
 */
var fs = require('fs');
var categoriesModel = require('../models/categories');


var categories = function(req, res, next)
{
    switch (req.method) {
        case 'GET': {

            categoriesModel.get().then(function(rows){
                if(rows)
                {
                    var rows = parseTree(rows);
                    res.json({success : true, rows : rows});
                }
                else
                {
                    res.json({success : false, mgs : 'Error!'});
                }
            });
            break;
        }
        case 'POST': {

            break;
        }

        case 'PUT':{



            break;
        }
    }
}

function parseTree(trees,result) 
{    
    result = result || getRootNodes(trees);
    
    for(var i in result)
    {
        var tree_node = result[i];
        
        var child_nodes = getChildNodes(trees,tree_node.id);
        if(child_nodes && child_nodes.length > 0)
        {
            result[i].children = parseTree(trees,child_nodes);
        }
    }
    return result;
}

function  getChildNodes(trees,parent_id)
{
    var result  = [];
    for (var i in trees) 
    {
        var tree_node = trees[i];
        if(tree_node.parent_id == parent_id)
        {
            result.push(tree_node);
        }
    }
    return result;
    
}

function getRootNodes(trees) 
{
    var root_nodes = [];
    for(var i in trees)
    {
        var tree_node = trees[i];
        if(tree_node.parent_id == 0)
        {
            root_nodes.push(tree_node);
        }
    }
    
    return root_nodes;
}

module.exports = categories;