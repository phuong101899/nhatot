var emailModel = require('../models/email');

var email = function(req, res, next)
{
    switch (req.method)
    {
        case 'POST' :
        {
            var data = req.body;

            // check duplicate via unique on database

            emailModel.add(data).then(function(result){
                if(result)
                {
                    return res.json({success : true, msg : 'Đăng kí email thành công!'});
                }

                return res.json({success : false, msg : 'E-mail này đã được đăng ký!'});

            });
            break;
        }
    }
};

module.exports = email;