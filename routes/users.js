var userModel = require('../models/users');
var articleModel = require('../models/article');
var md5 = require('md5');

var router = function (req, res, next) {
	if(req.query.type == 'article_list')
	{
		try{
			var user_data = JSON.parse(req.cookies['USER_LOGIN']);
			var user_id = user_data.id;
		}catch (e){
			return res.json({success : false, msg : "Can't not permission to access!"});
		}


		articleModel.getByUserId(user_id).then(function(rows){
			if(rows)
			{

				res.json({success : true, rows : rows});
			}
			else
			{
				res.json({success : false, msg : 'Error!'});
			}
		});
		return;
	}

	switch (req.method) {
		case 'GET': {
            userModel.get().then(function(rows){
                console.log(rows);
                res.json(rows);
            });
			break;
		}
		case 'POST': {
			var data = {
				username: req.body.username,
				password : md5(req.body.password),
				email : req.body.email
			};
			
			userModel.add(data, function (result) {
				if(result != false)
				{
					
					res.json({success : true, msg : 'Đăng ký thành công',data : result});
				}
				else
				{
					res.json({success : false, msg : 'Insert error'});
				}
			});
			
			break;
		}

		case 'PUT':{

			var username = req.body.username;
			var password = md5(req.body.password);

			userModel.login(username,password, function (result) {
				if(result)
				{
					var minute = 24 * 60 * 60 * 1000;
  					res.cookie('USER_LOGIN',JSON.stringify(result), { maxAge: minute });
					res.json({success : true,data : result,msg : 'Đăng nhập thành công!'});
				}
				else
				{
					res.json({success : false, msg : 'Tên tài khoản hoặc mật khẩu không đúng!'});
				}
			});

			break;
		}
	}




}

module.exports = router;
